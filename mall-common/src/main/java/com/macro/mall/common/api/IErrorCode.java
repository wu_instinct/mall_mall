package com.macro.mall.common.api;

/**
 * 封装API的错误码
 * Created by wenchang on 2020/7/19.
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}
